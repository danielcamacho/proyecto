const express = require('express')
const path = require('path')
const app = express()

//configuracion
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

//routes
app.use(require('./routes/index'))

//archivo estatico --> static files
app.use(express.static(path.join(__dirname, 'public')));

app.listen(8000, () => {
    console.log("Este servidor escucha por puerto 8000")
})