const express = require('express')
const router = express.Router()

// Se muestra la plantilla index.pug
router.get('/', (req, res) => {
    res.render('index', { tittle: 'Inicio' });
});

module.exports = router;